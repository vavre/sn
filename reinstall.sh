#!/bin/bash
# install.sh
# Installs smartnode on Ubuntu 16.04 LTS x64
# ATTENTION: The anti-ddos part will disable http, https and dns ports.

if [ "$(whoami)" != "root" ]; then
  echo "Script must be run as user: root"
  exit -1
fi

pID=$(ps -ef | grep smartcashd | awk '{print $2}')
kill ${pID}
rm -rf ~/.smartcash/

# Warning that the script will reboot the server
echo "WARNING: This script will reboot the server when it's finished."
#printf "Press Ctrl+C to cancel or Enter to continue: "
#read IGNORE


while getopts ":k:n:" opt; do
  case $opt in
    k) nodePK="$OPTARG"
    ;;
    n) nodePW="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done




cd


# Get a new privatekey by going to console >> debug and typing smartnode genkey
printf "SmartNode GenKey: ${nodePK}"
_nodePrivateKey="$nodePK"

# The RPC node will only accept connections from your localhost
_rpcUserName="smartuser"

# Choose a random and secure password for the RPC
_rpcPassword="$nodePW"

# Get the IP address of your vps which will be hosting the smartnode
_nodeIpAddress=$(ip route get 1 | awk '{print $NF;exit}')

# Make a new directory for smartcash daemon
mkdir ~/.smartcash/
touch ~/.smartcash/smartcash.conf

# Change the directory to ~/.smartcash
cd ~/.smartcash/

# Create the initial smartcash.conf file
echo "rpcuser=${_rpcUserName}
rpcpassword=${_rpcPassword}
rpcallowip=127.0.0.1
listen=1
server=1
daemon=1
logtimestamps=1
maxconnections=64
txindex=1
smartnode=1
smartnodeprivkey=${_nodePrivateKey}
" > smartcash.conf
cd

# Install smartcashd using apt-get
apt-get update -y
apt-get install software-properties-common -y
add-apt-repository ppa:smartcash/ppa -y && apt update -y && apt install smartcashd -y && smartcashd

# Reboot the server
reboot
